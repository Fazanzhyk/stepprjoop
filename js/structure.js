class Visit {
    constructor(id, visitName, date, fullName) {
        this.id = id;
        this.visitName = visitName;
        this.date = date;
        this.fullName = fullName;
    }

    /**
     * Писал его под Локал Сторедж, но так и не понял как применить правильно. Возможно не нужно совсем
     * @returns {number}
     */
    static getId () {
        return this.id = Math.random().toFixed(5)*1000000;
    }

    get Data () {
        return [this.id, this.visitName, this.date, this.fullName];
    }
}

class Cardiologist extends Visit {
    constructor(date, fullName, visitTarget, ordinaryPressure, weightIndex, sufferedDiseases) {
        super('Кардиолог', date, fullName);
        this.visitTarget = visitTarget;
        this.ordinaryPressure = ordinaryPressure;
        this.weightIndex = weightIndex;
        this.sufferedDiseases = sufferedDiseases;
    }

    get Data () {
        return [this.visitTarget, this.ordinaryPressure, this.weightIndex, this.sufferedDiseases];
    }

}

class Dentist extends Visit {
    constructor(date, fullName, visitTarget, lastVisitDate) {
        super('Cтоматолог', date, fullName);
        this.visitTarget = visitTarget;
        this.lastVisitDate = lastVisitDate;
    }

    get Data () {
        return [this.visitTarget, this.lastVisitDate];
    }

}

class Therapist extends Visit {
    constructor(date, fullName, visitTarget, age) {
        super('Терапевт', date, fullName);
        this.visitTarget = visitTarget;
        this.age = age;
    }

    get Data () {
        return [this.visitTarget, this.age];
    }
}