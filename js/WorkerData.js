class DataWorker {
    static get doctorBlankData() {
        return [
            {
                id: 'cardio',
                title: 'Кардиолог',
                fields: [
                    {title: 'Цель визита', type: 'text', id: 'visitTarget'},
                    {title: 'Обычное давление', type: 'text', id: 'ordinaryPressure'},
                    {title: 'Индекс массы тела', type: 'text', id: 'weightIndex'},
                    {title: 'Перенесенные заболевания сердечно-сосудистой системы', type: 'text', id: 'sufferedDiseases'},
                    {title: 'Возраст', type: 'number', id: 'age'},
                    {title: 'ФИО', type: 'text', id: 'fullName'}
                ]
            },

            {
                id: 'dantist',
                title: 'Стоматолог',
                fields: [
                    {title: 'Цель визита', type: 'text', id: 'visitTarget'},
                    {title: 'Дата последнего посещения', type: 'date', id: 'lastVisitDate'},
                    {title: 'ФИО', type: 'text', id: 'fullName'}
                ]
            },
            {
                id: 'therapist',
                title: 'Терапевт',
                fields: [
                    {title: 'Цель визита', type: 'text', id: 'visitTarget'},
                    {title: 'Возраст', type: 'number', id: 'age'},
                    {title: 'ФИО', type: 'text', id: 'fullName'}
                ]
            }
        ];
    }
}