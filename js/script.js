document.addEventListener('DOMContentLoaded', onReady);

let visit;
let found;

/**
 * Entry point
 * @desc Навешиваем обработчики событий
 */

function onReady () {
    const topBtn = document.getElementById('topBtn');
    let modal = document.getElementById('modal');
    let wrapper = document.getElementById('dark');
    const medicSelect = document.getElementById('medic-select');
    wrapper.addEventListener('click', onWrapperClick);
    topBtn.addEventListener('click', function onTopBtnClick () {
        modal.classList.add('active');
    });
    medicSelect.addEventListener('change', onMedicSelectChange);
    const createBtn = document.getElementById('create-btn');
    createBtn.addEventListener('click', onCreateBtnClick);
    const closeBtn = document.getElementById('closeBtn');
    closeBtn.addEventListener('click', function onCloseBtnClick () {
        modal.classList.remove('active');
    });
}

/**
 * Определяется событие для динамического создания инпутов
 * @param event
 */
function onMedicSelectChange (e) {
    let target = e.target;
    let value = target.value;
    let form = document.getElementById('select-form');
    let input = document.querySelectorAll('input');
    if (value === 'no-doctor') {
        for (let inp of input) {
            form.removeChild(inp);
        }
    }
    found = DataWorker.doctorBlankData.find(elem => value === elem.id);
    for (let inp of input) {
        form.removeChild(inp);
    }
    found.fields.forEach(item => {
        let input = document.createElement('input');
        input.id = item.id;
        input.type = item.type;
        input.setAttribute('required', '');
        input.placeholder = item.title;
        form.append(input);
    });
}

/**
 *  Событие по клику на кнопку создать Визит - создание карточки Визита к врачу
 *
 */
function onCreateBtnClick () {
    let modal = document.getElementById('modal');
    let sentence = document.getElementById('sentence');
    modal.classList.remove('active');
    sentence.classList.add('optional');
    let d = new Date();
    visit = new Visit (Visit.getId(),found.title, d.toLocaleDateString(), document.getElementById('fullName').value);

    if (found.title === 'Кардиолог')  {
        let cardiolog = new Cardiologist('', '', document.getElementById('visitTarget').value,
            document.getElementById('ordinaryPressure').value, document.getElementById('weightIndex').value,
            document.getElementById('sufferedDiseases').value);
        let dom = new DOM();
        dom.buildDoctorVisitCard(visit.Data, cardiolog.Data);
    }

    if (found.title === 'Терапевт') {
        let therapist = new Therapist('', '', document.getElementById('visitTarget').value,
            document.getElementById('age').value);
        let dom = new DOM();
        dom.buildDoctorVisitCard(visit.Data, therapist.Data);
    }

    if (found.title === 'Стоматолог') {
        let dantist = new Dentist('', '', document.getElementById('visitTarget').value,
            document.getElementById('lastVisitDate').value);
        let dom = new DOM();
        dom.buildDoctorVisitCard(visit.Data, dantist.Data);
    }
}

/**
 *
 * Закрытие модального окна по клику вне его области (много исключений)
 * @param event
 */
function onWrapperClick (ev) {
    ev.stopPropagation();
    let target = ev.target;
    const form = document.getElementById('select-form');
    let commentField = document.getElementById('comment-field');
    let visitTarget = document.getElementById('visitTarget');
    let ordinaryPressure = document.getElementById('ordinaryPressure');
    let weightIndex = document.getElementById('weightIndex');
    let sufferedDiseases = document.getElementById('sufferedDiseases');
    let age = document.getElementById('age');
    const topBtn = document.getElementById('topBtn');
    let fullName = document.getElementById('fullName');
    const medicSelect = document.getElementById('medic-select');
    let lastVisitDate = document.getElementById('lastVisitDate');
    let modal = document.getElementById('modal');
    if (modal !== target && topBtn !== target && form !== target && medicSelect !== target && commentField !== target
        && ordinaryPressure !== target && weightIndex !== target && sufferedDiseases !== target && age !== target
        && fullName !== target && lastVisitDate !== target && visitTarget !== target) {
        ev.stopPropagation();
        modal.classList.remove('active');
    }
}
