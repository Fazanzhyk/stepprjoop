let ul;

/**
 *
 * Класс, который создает карточку визита к врачу
 * @ params Visit Data (данные класса Визит) doctorData (данные выбранного доктора)
 */
class DOM {

    buildDoctorVisitCard(visitData, doctorData) {

        let visitCard = document.createElement('div');
        visitCard.classList.add('visit-card');
        let board = document.getElementById('main-div');
        let showMoreBtn = document.createElement('button');
        showMoreBtn.classList.add('cardInf');
        showMoreBtn.innerHTML = 'Show more';

        let closeVisitCard = document.createElement('button');
        closeVisitCard.classList.add('closeCard');
        closeVisitCard.innerHTML = 'X';

        visitCard.append(showMoreBtn);
        visitCard.append(closeVisitCard);
        let topUl = document.createElement('ul');


        visitCard.append(topUl);
        ul = document.createElement('ul');
        ul.classList.add('optional');

        visitData.forEach(item => {
            let list = document.createElement('li');
            list.innerHTML = item;
            topUl.append(list);
            ul.id = item.id;
        });

        showMoreBtn.addEventListener('click', function onShowMoreInformBtnClick() {
            let child = visitCard.lastChild;
            child.classList.toggle('active');
        });

        closeVisitCard.addEventListener('click', function onCloseVisitCardBtnClick() {
            visitCard.remove();
        });

        doctorData.forEach(item => {
            let list = document.createElement('li');
            list.innerHTML = item;
            ul.append(list);
        });

        visitCard.append(ul);
        let commentField = document.getElementById('comment-field');
        ul.append(commentField.value);
        board.append(visitCard);
    }
}